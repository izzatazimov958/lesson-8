// Your task is to design given task as it is. When clicking the button "START", it should start counting.

// If you click Pause button, it should save current state ( Count ) then save it to LocalStorage. Whenever the page is refreshed, it should continue from where it has stopped.

// When you click Reset button, state (Count) and LocalStorage should be cleaned.

// When you click Add button, you have to log the current state in list. And you have to manipulate the array of list. (CRUD)

const addBtn = document.getElementById("add");
const resetBtn = document.getElementById("reset");
const pauseBtn = document.getElementById("pause");

let hrhtml = document.querySelector(".hour");
let minhtml = document.querySelector(".minute");
let sechtml = document.querySelector(".second");
const logsList = document.getElementById("logsList");

let hr = 0;
let min = 0;
let sec = 0;
let startTimer;
let orderLogs = 0;

let logs = document.querySelector(".boxUl");
let isPaused = false;

function add() {
  save();
}

function start() {
  startTimer = setInterval(() => {
    ++sec;
    sec = sec < 10 ? "0" + sec : sec;
    if (sec == 60) {
      sec = "0" + 0;
      minFunc();
    }
    sechtml.innerHTML = sec;
    localStorage.setItem("sec", sec);

    function minFunc() {
      min++;
      min = min < 10 ? "0" + min : min;
      if (min == 60) {
        min = "0" + 0;
        minhtml.innerHTML = "00";
        hrFunc();
      }
      minhtml.innerHTML = min;
      localStorage.setItem("min", min);
      console.log(min);
    }

    function hrFunc() {
      hr++;
      hr = hr < 10 ? "0" + hr : hr;
      if (hr == 24) {
        min = "0" + 0;
        minhtml.innerHTML = "00";
      }
      hrhtml.innerHTML = hr;
      localStorage.setItem("hr", hr);
    }
  }, 1000);
}

function pause() {
  clearInterval(startTimer);
}

function reset() {
  clearInterval(startTimer);
  hr = "0" + 0;
  min = "0" + 0;
  sec = "0" + 0;
  hrhtml.innerHTML = hr;
  minhtml.innerHTML = min;
  sechtml.innerHTML = sec;
  localStorage.clear();
  logsList.innerHTML = "";
  pauseBtn.innerText = "Start";
  orderLogs = 0;
  localStorage.setItem("pause", false);
  localStorage.setItem("pausebtn.innerText", "Start");
  localStorage.setItem("logs", logsList.innerHTML);
}

function save() {
  orderLogs++;
  logsList.innerHTML += `<li><span class="orders">${orderLogs}.</span><span>${hrhtml.innerHTML}:${minhtml.innerHTML}:${sechtml.innerHTML}</span><button class="remove">&#10005;</button></li>`;

  // document.querySelectorAll(".remove").forEach((btn, index) => {
  //   btn.addEventListener("click", () => {
  //     // btn.parentElement.remove();
  //     logsList.removeChild(btn.parentElement);
  //   });
  // });
}

logsList.addEventListener("click", (e) => {
  if (e.target.classList.contains("remove")) {
    e.target.parentElement.remove();
  }
});

pauseBtn.addEventListener("click", () => {
  if (isPaused) {
    pause();
    console.log("pause bosildi");

    isPaused = false;
    pauseBtn.innerText = "Resume";
    localStorage.setItem("pauseBtn.innerHTML", "Resume");
    localStorage.setItem("isPaused", false);
  } else {
    start();
    console.log("start bosildi");
    isPaused = true;
    pauseBtn.innerText = "Pause";
    localStorage.setItem("pauseBtn.innerHTML", "Pause");
    localStorage.setItem("isPaused", true);
  }
});

addBtn.addEventListener("click", () => {
  let result = +sec + +min + +hr;
  if (result > 0) {
    add();
  } else {
    alert("Please start the timer");
  }
});

resetBtn.addEventListener("click", reset);

window.onbeforeunload = () => {
  // save the state before leaving the page
  localStorage.setItem("logs", logsList.innerHTML);
  localStorage.setItem("orderLogs", orderLogs);

  // [...logsList.children].forEach((li, index) => {
  //   localStorage.setItem(`${index}`, li.innerHTML);
  // });
};

window.onload = () => {
  if (localStorage.getItem("hr") != null) {
    hr = localStorage.getItem("hr");
    hrhtml.innerHTML = hr;
  }
  if (localStorage.getItem("min") != null) {
    min = localStorage.getItem("min");
    minhtml.innerHTML = min;
  }
  if (localStorage.getItem("sec") != null) {
    sec = localStorage.getItem("sec");
    sechtml.innerHTML = sec;
  }

  if (localStorage.getItem("logs") != null) {
    logsList.innerHTML = localStorage.getItem("logs");
  }

  let checknumbers = sechtml.innerHTML,
    checknumbers2 = minhtml.innerHTML,
    checknumbers3 = hrhtml.innerHTML;

  let result = +checknumbers + +checknumbers2 + +checknumbers3;
  if (result > 0) {
    pauseBtn.innerText = "Resume";
  }

  console.log(result, "result");

  if (localStorage.getItem("orderLogs") != null) {
    orderLogs = localStorage.getItem("orderLogs");
  }
};
